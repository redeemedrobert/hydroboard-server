// Compile/assemble with g++ using: g++ -I ./ -c main.cpp hydroboardserver.cpp configureserver.cpp db.cpp -pthread -l sqlite3
// Link and output to .exe with g++ using: g++ -o hydroboard.exe main.o hydroboardserver.o configureserver.o db.o -pthread -l sqlite3
// Start server with ./hydroboard.exe
// Written by Robert Robinson
// Last modified 2021002230930

#define SERVER_PORT 80

#include <iostream>
#include "hydroboardserver.h"

int main () {

    std::cout << "Starting hydroboard server on port " << SERVER_PORT << "\n";

    HydroboardServer svr(SERVER_PORT);
    svr.startServer();

    return 0;

}