#include <iostream>
#include <string>
#include "httplib.h"

std::string getDb(const std::string& name, int id=-1, int year = -1, int month = -1, int date = -1, int hour = -1, int minute = -1);
std::string putDb(const std::string& name, int id=-1, int year = -1, int month = -1, int date = -1, int hour = -1, int minute = -1);
std::string delDb(int id=-1);

void configureServer(httplib::Server& svr) {

    std::cout << "Configuring mountpoint...";

    svr.set_mount_point("/", "./www");
    std::cout << "\t\t\t\e[32mOK\e[0m\n";

    std::cout << "Configuring GET requests...";
    svr.Get("/api/get",
        [](const httplib::Request& req, httplib::Response &res) {
            if(
                (req.has_param("id") && req.get_param_value("id") == "")
                || (req.has_param("name") && req.get_param_value("name")        == "")
                || (req.has_param("year") && req.get_param_value("year")        == "")
                || (req.has_param("month") && req.get_param_value("month")      == "")
                || (req.has_param("date") && req.get_param_value("date")        == "")
                || (req.has_param("hour") && req.get_param_value("hour")        == "")
                || (req.has_param("minute") && req.get_param_value("minute")    == "")
            )
                res.set_content("{\"error\":\"request included a parameter with blank value\"}", "application/json");
            else
                res.set_content(
                    
                    getDb(
                        req.has_param("name")   ? req.get_param_value("name")               : "",
                        req.has_param("id")     ? std::stoi(req.get_param_value("id"))      : -1,
                        req.has_param("year")   ? std::stoi(req.get_param_value("year"))    : -1,
                        req.has_param("month")  ? std::stoi(req.get_param_value("month"))   : -1,
                        req.has_param("date")   ? std::stoi(req.get_param_value("date"))    : -1,
                        req.has_param("hour")   ? std::stoi(req.get_param_value("hour"))    : -1,
                        req.has_param("minute") ? std::stoi(req.get_param_value("minute"))  : -1
                    ),
                    "application/json"

                );

        }
    );
    std::cout << "\t\t\t\e[32mOK\e[0m\n";

    std::cout << "Configuring PUT requests...";
    svr.Put("/api/put",
        [](const httplib::Request& req, httplib::Response &res) {

            if(
                (req.has_param("id") && req.get_param_value("id") == "")
                || (req.has_param("name") && req.get_param_value("name")        == "")
                || (req.has_param("year") && req.get_param_value("year")        == "")
                || (req.has_param("month") && req.get_param_value("month")      == "")
                || (req.has_param("date") && req.get_param_value("date")        == "")
                || (req.has_param("hour") && req.get_param_value("hour")        == "")
                || (req.has_param("minute") && req.get_param_value("minute")    == "")
            )
                res.set_content("{\"error\":\"request included a parameter with blank value\"}", "application/json");
            else {

                int args = false;
                if(req.has_param("id")) {

                    if(req.has_param("name"))   args = true;
                    if(req.has_param("year"))   args = true;
                    if(req.has_param("month"))  args = true;
                    if(req.has_param("date"))   args = true;
                    if(req.has_param("hour"))   args = true;
                    if(req.has_param("minute")) args = true;
                    if(!args)
                        res.set_content("{\"error\":\"an id was given without any other parameters\"}", "application/json");

                }
                if(!req.has_param("id") || args == true)

                    res.set_content(
                        
                        putDb(
                            req.has_param("name")   ? req.get_param_value("name")               : "",
                            req.has_param("id")     ? std::stoi(req.get_param_value("id"))      : -1,
                            req.has_param("year")   ? std::stoi(req.get_param_value("year"))    : -1,
                            req.has_param("month")  ? std::stoi(req.get_param_value("month"))   : -1,
                            req.has_param("date")   ? std::stoi(req.get_param_value("date"))    : -1,
                            req.has_param("hour")   ? std::stoi(req.get_param_value("hour"))    : -1,
                            req.has_param("minute") ? std::stoi(req.get_param_value("minute"))  : -1
                        ),
                        "application/json"

                    );
            }
        }
    );
    std::cout << "\t\t\t\e[32mOK\e[0m\n";

    std::cout << "Configuring DELETE requests...";
    svr.Delete("/api/delete",
        [](const httplib::Request& req, httplib::Response &res) {

            if(!req.has_param("id"))
                res.set_content("{\"error\":\"request must include the id parameter\"}", "application/json");
            else if(req.get_param_value("id") == "")
                res.set_content("{\"error\":\"request id may not be blank\"}", "application/json");
            else
                res.set_content(
                    
                    delDb(
                        std::stoi(req.get_param_value("id"))
                    ),
                    "application/json"

                );
            
        }
    );
    std::cout << "\t\t\t\e[32mOK\e[0m\n";

}