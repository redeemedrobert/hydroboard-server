#include "sqlite3.h"
#include <iostream>
#include <string>

sqlite3* db;

void initDb() {

    std::cout << "Initializing database...";

    std::string createTable = 
        "CREATE TABLE IF NOT EXISTS hydroboard("
        "id     INTEGER PRIMARY KEY,"
        "name   TEXT    NOT NULL,"
        "year   INT     NOT NULL,"
        "month  INT     NOT NULL,"
        "date   INT     NOT NULL,"
        "hour   INT     NOT NULL,"
        "minute INT     NOT NULL)" 
    ;

    int exit = sqlite3_open("hydroboard.db", &db);
    char* errMsg;
    if(exit) {
        std::cout << "\t\t\t\e[31mERROR OPENING DATABASE\e[0m\n";
        std::exit(1);
    }

    exit = sqlite3_exec(db, createTable.c_str(), NULL, 0, &errMsg);
    if(exit != SQLITE_OK) {
        std::cout << "\t\t\t\e[31mERROR CREATING TABLE: " << errMsg << "\e[0m\n";
        std::exit(1);
    }

    std::cout << "\t\t\t\e[32mOK\e[0m\n";

}

std::string getDb(const std::string& name, int id=-1, int year = -1, int month = -1, int date = -1, int hour = -1, int minute = -1) {
    
    sqlite3_stmt* stmt;
    std::string rslt;
    std::string query = "SELECT * FROM hydroboard ";
    bool args = false;

    if(id != -1 || name.length() > 0 || year != -1 || month != -1 || date != -1 || hour != -1 || minute != -1)
        query += "WHERE ";

    if(name.length() > 0)   { query += "name=:name ";                                   args = true; }
    if(id != -1)            { query += args ? "AND id=:id "         : "id=:id ";        args = true; }
    if(year != -1)          { query += args ? "AND year=:year "     : "year=:year ";    args = true; }
    if(month != -1)         { query += args ? "AND month=:month "   : "month=:month ";  args = true; }
    if(date != -1)          { query += args ? "AND date=:date "     : "date=:date ";    args = true; }
    if(hour != -1)          { query += args ? "AND hour=:hour "     : "hour=:hour ";    args = true; }
    if(minute != -1)        { query += args ? "AND minute=:minute"  : "minute=:minute";              }

    int prep = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    if(prep == SQLITE_ERROR)
        return
            "{"
                "\"error\":\"preperror\","
                "\"msg\":\"" + std::string(sqlite3_errmsg(db)) + "\""
            "}"
        ;

    if(name.length() > 0)   sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, ":name"), name.c_str(), name.length(), SQLITE_STATIC);
    if(id != -1)            sqlite3_bind_int(stmt,  sqlite3_bind_parameter_index(stmt, ":id"),       id      );
    if(year != -1)          sqlite3_bind_int(stmt,  sqlite3_bind_parameter_index(stmt, ":year"),     year    );
    if(month != -1)         sqlite3_bind_int(stmt,  sqlite3_bind_parameter_index(stmt, ":month"),    month   );
    if(date != -1)          sqlite3_bind_int(stmt,  sqlite3_bind_parameter_index(stmt, ":date"),     date    );
    if(hour != -1)          sqlite3_bind_int(stmt,  sqlite3_bind_parameter_index(stmt, ":hour"),     hour    );
    if(minute != -1)        sqlite3_bind_int(stmt,  sqlite3_bind_parameter_index(stmt, ":minute"),   minute  );

    rslt =
        "{"
            "\"reqName\":\""    + name + "\","
            "\"reqId\":\""      + std::to_string(id)    + "\","
            "\"reqYear\":\""    + std::to_string(year)    + "\","
            "\"reqMonth\":\""   + std::to_string(month)   + "\","
            "\"reqDate\":\""    + std::to_string(date)    + "\","
            "\"reqHour\":\""    + std::to_string(hour)    + "\","
            "\"reqMinute\":\""  + std::to_string(minute)  + "\","
            "\"results\":["
    ; 

    while(sqlite3_step(stmt) == SQLITE_ROW) {
        rslt += "{";
        for(int i=0; i < sqlite3_column_count(stmt); ++i) {
            rslt += "\"" + std::string(sqlite3_column_name(stmt, i)) + "\":\"" + std::string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, i))) + "\"";
            if(i != sqlite3_column_count(stmt)-1) rslt += ",";
        }
        rslt += "},";
    }

    if(rslt.back() == ',') rslt.pop_back(); // can't have a comma at the end of the results array
    rslt += "]}";

    return rslt;

}

std::string putDb(const std::string& name, int id=-1, int year=-1, int month=-1, int date=-1, int hour=-1, int minute=-1) {
    
    std::string query;
    sqlite3_stmt* stmt;

    if(id == -1) {
        
        if(name == "" || year == -1 || month == -1 || date == -1 || hour == -1 || minute == -1) {
            std::string err = "{ \"error\": \"INCOMPLETE DATA FOR DB INSERT: ";
            if(name == "")      err += "name "  ;
            if(year == -1)      err += "year "  ;
            if(month == -1)     err += "month " ;
            if(date == -1)      err += "date "  ;
            if(hour == -1)      err += "hour "  ;
            if(minute == -1)    err += "minute ";
            err += "\"}";
            return err;
        }

        query = "INSERT INTO hydroboard(name, year, month, date, hour, minute) VALUES(?,?,?,?,?,?)";
        sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
        sqlite3_bind_text(stmt, 1, name.c_str(), -1, NULL);
        sqlite3_bind_int(stmt, 2, year);
        sqlite3_bind_int(stmt, 3, month);
        sqlite3_bind_int(stmt, 4, date);
        sqlite3_bind_int(stmt, 5, hour);
        sqlite3_bind_int(stmt, 6, minute);

        if(sqlite3_step(stmt) != SQLITE_DONE)
            return "{\"error\":\"" + std::string(sqlite3_errmsg(db)) + "\"}";

    } else {

        query = "SELECT id FROM hydroboard WHERE id=?";
        sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
        sqlite3_bind_int(stmt, 1, id);
        if(sqlite3_step(stmt) != SQLITE_ROW)
            return "{\"error\":\"no row exists with id " + std::to_string(id) + "\"}";

        int args = false;
        query = "UPDATE hydroboard SET ";
        if(name != "")      { query += "name=:name"; args = 1; }
        if(year != -1)      { if(args != 0) query += ","; query += "year=:year";    args = true; }
        if(month != -1)     { if(args != 0) query += ","; query += "month=:month";  args = true; }
        if(date != -1)      { if(args != 0) query += ","; query += "date=:date";    args = true; }
        if(hour != -1)      { if(args != 0) query += ","; query += "hour=:hour";    args = true; }
        if(minute != -1)    { if(args != 0) query += ","; query += "minute=:minute";             }
        query += " WHERE id=:id";

        sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, NULL);
        if(name != "")      sqlite3_bind_text(stmt, 1, name.c_str(), -1, NULL); // index 1 is given because name will always be the first parameter if it is present
        if(year != -1)      sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":year"),     year);
        if(month != -1)     sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":month"),    month);
        if(date != -1)      sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":date"),     date);
        if(hour != -1)      sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":hour"),     hour);
        if(minute != -1)    sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":minute"),   minute);
        sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, ":id"), id); // id is always present, no if needed

        if(sqlite3_step(stmt) != SQLITE_DONE)
            return "{\"error\":\"" + std::string(sqlite3_errmsg(db)) + "\"}";
        
    }

    return "{"
        "\"name\":\""   + name + "\","
        "\"year\":\""   + std::to_string(year)      + "\","
        "\"month\":\""  + std::to_string(month)     + "\","
        "\"date\":\""   + std::to_string(date)      + "\","
        "\"hour\":\""   + std::to_string(hour)      + "\","
        "\"minute\":\"" + std::to_string(minute)    + "\","
        "\"id\": \""    + (id == -1 ? std::to_string(sqlite3_last_insert_rowid(db)) : std::to_string(id)) + "\""
    "}";
    
}

std::string delDb(int id=-1) {
    
    if(id == -1)
        return "{\"error\":\"delete requests must include an id\"}";

    sqlite3_stmt* stmt;
    std::string query = "SELECT id FROM hydroboard WHERE id=?";
    sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
    sqlite3_bind_int(stmt, 1, id);
    if(sqlite3_step(stmt) != SQLITE_ROW)
        return "{\"error\":\"no row with id " + std::to_string(id) + "\"}";   
    
    query = "DELETE FROM hydroboard WHERE id=" + std::to_string(id);
    char* err;
    int exit = sqlite3_exec(db, query.c_str(), NULL, nullptr, &err);
    if (exit == SQLITE_OK)
        return "{\"delete\":\"" + std::to_string(id) + "\"}";
    else
        return "{\"error\":\"" + std::string(err) + "\"}";

}