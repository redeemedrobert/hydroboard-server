#ifndef HYDROBOARD_SERVER_H
#define HYDROBOARD_SERVER_H

#include "httplib.h"

class HydroboardServer {
    public:
        HydroboardServer(int port);
        void startServer();
        int usePort;
        httplib::Server hydroboardServer;

    private:
        void setupServer();
};

#endif