# Hydroboard Server

Web backend written in C++ using Yuji Hirose's cpp-httplib (https://github.com/yhirose/cpp-httplib).
The server is used by the Clare Controls QA team to track how much water we drink individually, in an effort to get all of us to drink a healthy amount (a lot) of water each working day.

Server is started from command line and its API is accessed via http://localhost/api.

All other requests are handled via a server filesystem mountpoint.

All application/website files go into the ./www directory.

## API Documentation
Table columns: id, name, year, month, date, hour, minute

All results are in JSON format, the response mimetype is application/json.
The id column is an unique integer (INTEGER PRIMARY KEY) used to identify each individual column.
The name column is TEXT and the rest are INTEGER.

### Retrieve rows
localhost/api/get will retrieve all rows from the table.

Retrieve specific row: localhost/api/get?id=123
* Any column name or combination of column names can be passed as a parameter. The server will only return rows with absolute matches, case sensitive
* The response JSON will include each requested parameter in the form of "reqName", "reqId", "reqYear" and so on, and a results array which will include an object fo reach row retrieved. Any parameter that was not specifically included in the search filter will be included with a -1, "reqId":"-1", or an empty string for "name" as in "reqName":"". The JSON response will look like this for the request localhost/api/get?year=2021&month=2&date=25: 
    * { "reqName":"", "reqId":"-1", "reqYear":"2021", "reqMonth":"2", "reqDate":"25","reqHour":"-1","reqMinute":"-1", "results": [{"id":"1","name":"User Name","year":"2021","month":"2","date":"25","hour":"8","minute":"30"},{"id":"1","name":"User Name","year":"2021","month":"2","date":"25","hour":"14","minute":"15"}]}

### Insert row
localhost/api/put?name=Name Here&year=2021&month=2&date=24&hour=13&minute=39
* An error will be returned if any of those parameters are not present.
* A valid request will return:
    * { "id":"5", "name": "Name Here", "year": "2021", "month": "2", "date": "24", "hour":"13", "minute": "39" }
    * Note that the returned ID is going to be that of the newly created row.

### Update row
Same as to insert a row, except include the id parameter, as in:
localhost/api/put?id=3&name=New Name&minute=15
* Only the parameters that are to be changed need to be included in the URI. Requesting to update an invalid ID will do nothing.
* In the JSON response, any parameter that was not included in the request will be returned as a -1 (name will be an empty string instead) to indicate that it was not changed, for instance:
    * localhost/api/put?id=5&name=Sponsbob Vierkantebroek&hour=14&minute=0 will return:
        * { "id":"5", "name": "Sponsbob Vierkantebroek", "year": "-1", "month": "-1", "date": "-1", "hour":"14", "minute": "0" }

### Delete row
localhost/api/delete?id=4
* Will return an error if a row with the given id does not exist, or if no id is given.
* If the row exists, the server will return a JSON object with a single property, delete, set to the given id.