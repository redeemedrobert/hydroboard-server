#include <iostream>
#include "hydroboardserver.h"
#include "httplib.h"

void configureServer(httplib::Server& svr);
void initDb();

HydroboardServer::HydroboardServer(int port) {

    std::cout << "Initializing server...\n";
    usePort = port;
    HydroboardServer::setupServer();

}

void HydroboardServer::setupServer() {

    initDb();

    std::cout << "Configuring server...\n";

    configureServer(hydroboardServer);

    // hydroboardServer.set_logger(
    //     [](const auto& req, const auto& res) {
    //         //setLogger(&req, &res);
    //     }
    // );

}

void HydroboardServer::startServer() {
    
    std::cout << "Listening on port " << usePort << "\n";
    hydroboardServer.listen("0.0.0.0", usePort);

}